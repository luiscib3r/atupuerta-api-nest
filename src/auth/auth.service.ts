import { Injectable } from '@nestjs/common';

import { UserService } from '../user/user.service';
import { ReturnUserDTO } from '../user/dto/user.dto';
import { JwtService } from '@nestjs/jwt';

import * as bcrypt from 'bcrypt'

@Injectable()
export class AuthService {

    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) { }

    async validateUser(username: string, pass: string) {
        const user = await this.userService.getUser(username);

        const match = await bcrypt.compare(pass, user.password)

        if(match)
            return new ReturnUserDTO(user)        

        return null
    }

    async login(user: any) {
        const payload = { username: user.username, sub: user.id }

        return {
            accessToken: this.jwtService.sign(payload)
        }
    }
}
