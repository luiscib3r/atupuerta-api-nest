import { Document } from 'mongoose';

export interface User extends Document {
    username: string
    firstName: string
    lastName: string
    password: string
    phoneNumber: string
    movilNumber: string
}