import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDTO {
    @ApiProperty({
        description: 'Login username',
        required: true
    })
    username: string

    @ApiProperty({
        description: 'First name',
        required: true
    })
    firstName: string

    @ApiProperty({
        description: 'Last name',
        required: true
    })
    lastName: string

    @ApiProperty({
        description: 'Login password',
        required: true
    })
    password: string

    @ApiProperty({
        description: 'Not movil phone number',
        required: false
    })
    phoneNumber: string

    @ApiProperty({
        description: 'Movil phone number',
        required: false
    })
    movilNumber: string
}

export class ReturnUserDTO {
    constructor(user: CreateUserDTO) {
        this.id = (user as any)._id
        this.username = user.username
        this.firstName = user.firstName
        this.lastName = user.lastName
        this.movilNumber = user.movilNumber
        this.phoneNumber = user.phoneNumber
    }

    @ApiProperty({
        description: 'User id',
        required: true
    })
    id: string

    @ApiProperty({
        description: 'Login username',
        required: true
    })
    username: string

    @ApiProperty({
        description: 'First name',
        required: true
    })
    firstName: string

    @ApiProperty({
        description: 'Last name',
        required: true
    })
    lastName: string

    @ApiProperty({
        description: 'Not movil phone number',
        required: false
    })
    phoneNumber: string

    @ApiProperty({
        description: 'Movil phone number',
        required: false
    })
    movilNumber: string
}