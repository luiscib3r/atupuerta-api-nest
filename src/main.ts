import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('ATuPuerta API')
    .setDescription('ATuPuerta API Description')
    .setVersion('1.0')
    .addBearerAuth()
    .build()

  const document = SwaggerModule.createDocument(app, options, {
    include: [AuthModule, UserModule]
  })

  const authDocument = SwaggerModule.createDocument(app, options, {
    include: [AuthModule]
  })

  const userDocument = SwaggerModule.createDocument(app, options, {
    include: [UserModule]
  })

  SwaggerModule.setup('docs', app, document)
  SwaggerModule.setup('docs/auth', app, authDocument)
  SwaggerModule.setup('docs/user', app, userDocument)

  await app.listen(process.env.PORT || 5000);
}
bootstrap();
